#!/usr/bin/python
# -*- coding: utf-8 -*-
import paho.mqtt.publish as mpub
import paho.mqtt.client as mcli
import sys, json
import datetime as dt
 
device = "kiuchi-device-test"
hostname = "rabbitmq.mkhome"
topicname = "amq/topic"
#auth = {
#  'username': hostname + "/" + device,
#  'password': 'SharedAccessSignature sr=kawano-iothub.azure-devices.net%2Fdevices%2Fkawano-device-auto73&sig=1%2FIw6h1%2FgaLkaPSA1iShvA8Ugr0000000000&se=1468986686'
#}
auth = {}
 
data = {
       "name" : "microsoft japan",
       "datetime" : dt.datetime.now().strftime("%Y%m%dT%H%M%S")
     }

def on_connect(mqttc, obj, rc):
    mqttc.subscribe("$SYS/#", 0)
    #print("rc: "+str(rc))
    sys.stderr.write("rc: "+str(rc)+"\n")

def on_message(mqttc, obj, msg):
    #print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
    sys.stderr.write(msg.topic+" "+str(msg.qos)+" "+str(msg.payload)+"\n")

def on_publish(mqttc, obj, mid):
    #print("Published message-id: "+str(mid))
    sys.stderr.write("Published message-id: "+str(mid)+"\n")

def on_log(mqttc, obj, level, string):
    #print(string)
    sys.stderr.write(string+"\n")

 
if __name__ == '__main__':
  mqttc = mcli.Client(protocol="MQTTv311")
  mqttc.username_pw_set("guest", "guest")
  mqttc.on_message = on_message
  mqttc.on_connect = on_connect
  mqttc.on_publish = on_publish

  mqttc.connect("rabbitmq.mkhome", 1883, 60)
  sys.stderr.write("connected\n")

  cnt=0
  while 1:
    now = dt.datetime.now().strftime("%Y%m%d%H%M%S.") + "%03d" % (dt.datetime.now().microsecond // 1000)
    data = '{"name": "Creationline MQTT Client", "date": "' + now + '", "count": ' + str(cnt) + '"}'
    mqttc.publish(topicname, data, 0)
    cnt+=1
  sys.stderr.write("send\n")

  mqttc.disconnect()
  sys.stderr.write("disconnected\n")
