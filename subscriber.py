#!/usr/bin/python
# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
import sys

host = '0.0.0.0'
port = 1883
topic = 'amq/topic'

def on_connect(client, userdata, flags, respons_code):
    #print('status {0}'.format(respons_code))
    sys.stderr.write('status {0}'.format(respons_code)+"\n")

    client.subscribe(topic)

def on_message(client, userdata, msg):
    #print(msg.topic + ' ' + str(msg.payload))
    sys.stderr.write(msg.topic + ' ' + str(msg.payload)+"\n")
    return 0

if __name__ == '__main__':

    # Publisherと同様に v3.1.1を利用
    client = mqtt.Client(protocol=mqtt.MQTTv311)

    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(host, port=port, keepalive=60)

    # 待ち受け状態にする
    client.loop_forever()
